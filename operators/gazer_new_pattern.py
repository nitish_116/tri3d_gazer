def main_new_pattern(context,folder_name):
    import bpy
    from imp import reload
    import utils_main
    reload(utils_main)

    from utils_main import obj_activate,hide_rest,clear_data
    # file_path = bpy.data.filepath
    import os,sys,inspect
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()

    pattern_folder = BASE_DIR + 'settings_config/' + str(bpy.context.scene.garment_folder) + '/pattern/' + folder_name

    os.system("mkdir " + pattern_folder)
    sample_folder_template = BASE_DIR + 'samples/garment_template/pattern/default/'
    os.system("cp -R "+sample_folder_template+"* " + pattern_folder)

    bpy.context.scene.pattern_folder = folder_name


def generate_pattern_folder_name():
    
    import os,sys,inspect
    from imp import reload
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    import bpy
    dir_name = BASE_DIR + 'settings_config/' + bpy.context.scene.garment_folder + '/pattern/'
    name_found = False

    cnt = 0
    cur_garment_name = ''
    
    if os.path.exists(dir_name):
        while not name_found:
            cur_garment_name = 'pattern' + str(cnt)
            if cur_garment_name not in os.listdir(dir_name):
                name_found = True
            cnt+=1

    return cur_garment_name


    
class NewPattern(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.new_pattern"
    bl_label = "GAZER NEW PATTERN"
    bl_options = {'REGISTER', 'UNDO'}  

    g_folder_name = generate_pattern_folder_name()
    folder_name = bpy.props.StringProperty(default=g_folder_name) 

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        self.folder_name = generate_pattern_folder_name()
        main_new_pattern(context,self.folder_name)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(NewPattern)

def unregister():
    bpy.utils.unregister_class(NewPattern)

if __name__ == "__main__":
    register()
    # # test call
    # bpy.ops.gazer.create_pattern()
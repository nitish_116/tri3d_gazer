def fill_pattern_mesh():
    #update the edge points , inputs will be edge_points_dict and nothing else , read in the 
    #outline.blend and add edge points and push it to mesh.blend in garment folder 

    #add vertex_group_names to the newly created points , edge_vertex groups 

    #call meshpy , get the new points , create the mesh accordingly 

    #add _all vertex group for the newly created points 
    pass
def main_mesh_filler(context):
    import os,sys,inspect,importlib
    from imp import reload
    import bpy,copy
    from pprint import pprint
    from zodbpickle import pickle

    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    UTILS_DIR = BASE_DIR + 'utils/'
    for cur_util_py in os.listdir(UTILS_DIR):
        if 'utils' in cur_util_py and '.py' in cur_util_py:
            cur_util  = cur_util_py.split(".")[0]
            try:
                class_module = importlib.import_module(cur_util)
                reload(class_module)
            except ImportError:
                pass

    from utils_path_definitions import get_dynamic_properties,get_pattern_list
    dp = get_dynamic_properties(BASE_DIR)
    bpy.ops.gazer.adjust_edge_points()
    springs_dict = {}
    if os.path.exists(dp['springs_dict_pickle_path']):
        springs_dict = pickle.load(open(dp['springs_dict_pickle_path'],'rb'))

    edge_points_dict = {}
    if os.path.exists(dp['edge_points_pickle_path']):
        edge_points_dict = pickle.load(open(dp['edge_points_pickle_path'],'rb'))

    cnt = 0
    for cur_pattern in get_pattern_list(BASE_DIR):
        if 1:#cnt == 0:
            bpy.context.scene.pattern_folder = cur_pattern
            dp = get_dynamic_properties(BASE_DIR)
            #Will be called for each pattern, looped over all the patterns 
            #Requirement is to create the mesh.blend for all the patterns and push them to one common blend file , need to take care of giving vertex groups names as well 
            # fill_pattern_mesh(cur_pattern,dp,springs_dict,edge_points_dict)
            bpy.ops.gazer.create_pattern_edge_points()
        cnt+=1

    
    

    pass

class MeshFiller(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.mesh_filler"
    bl_label = "GAZER MESH FILLER"
    bl_options = {'REGISTER', 'UNDO'}  

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        main_mesh_filler(context)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(MeshFiller)

def unregister():
    bpy.utils.unregister_class(MeshFiller)

if __name__ == "__main__":
    register()
    # # test call
    # bpy.ops.gazer.mesh_filler()
def main_update_pattern(context,basic_unit,max_volume):
    import bpy
    from imp import reload
    import utils_main,utils_pattern
    reload(utils_main)
    reload(utils_pattern)

    from utils_main import obj_activate,hide_rest,clear_data
    # file_path = bpy.data.filepath
    import os,sys,inspect
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    cur_garment_folder = bpy.context.scene.garment_folder
    cur_pattern_folder = bpy.context.scene.pattern_folder

    settings_config_folder = BASE_DIR + "settings_config/" + cur_garment_folder + '/pattern/' + cur_pattern_folder + '/'
    config_path =  settings_config_folder + 'config_initial_dicts_pattern.py'
    outline_blend_path = settings_config_folder + 'outline.blend'

    # fill_config_pattern(config_path)
    current_file_path = bpy.data.filepath
    bpy.ops.wm.save_mainfile()    
    from utils_main import os_blend_file
    os_blend_file(current_file_path,outline_blend_path)

    cnt = 0
    for ob in bpy.data.objects:
        if ob.select:
            ob.select = False
            ob.name = str(cur_pattern_folder)
        else:
            ob.select = True
            cnt+=1
    if cnt > 0:
        bpy.ops.object.delete(use_global=False)

    from utils_pattern import assign_edge_vertex_groups,assign_corner_vertex_groups
    from utils_main import obj_activate
    ob = obj_activate(cur_pattern_folder)

    ob['basic_unit'] = basic_unit
    ob['max_volume'] = max_volume
    ob['gazer_type'] = 'pattern'

    for vg in ob.vertex_groups:
        ob.vertex_groups.remove(vg)
        
    ob = assign_corner_vertex_groups(ob)
    ob = assign_edge_vertex_groups(ob)

    

    bpy.ops.wm.save_mainfile()
    
    os_blend_file(current_file_path,current_file_path)
    bpy.context.active_object.name = cur_pattern_folder

class UpdatePattern(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.update_pattern"
    bl_label = "GAZER UPDATE PATTERN"
    bl_options = {'REGISTER', 'UNDO'}  


    basic_unit = bpy.props.FloatProperty(default=bpy.context.scene.basic_unit)
    max_volume = bpy.props.FloatProperty(default=bpy.context.scene.max_volume)
    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        main_update_pattern(context,self.basic_unit,self.max_volume)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(UpdatePattern)

def unregister():
    bpy.utils.unregister_class(UpdatePattern)

if __name__ == "__main__":
    register()
    # # test call
    # bpy.ops.gazer.update_pattern()
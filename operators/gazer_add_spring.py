def main_add_spring(context,internal_reverse,internal_add):
    import bpy
    from imp import reload
    import utils_main,utils_pattern,utils_garment
    reload(utils_main)
    reload(utils_pattern)
    reload(utils_garment)

    from utils_main import obj_activate,hide_rest,clear_data
    from utils_garment import add_one_spring
    # file_path = bpy.data.filepath
    import os,sys,inspect
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    cur_garment_folder = bpy.context.scene.garment_folder
    cur_pattern_folder = bpy.context.scene.pattern_folder

    garment_config_folder = BASE_DIR + "settings_config/" + cur_garment_folder + '/garment/default/'
    pattern_config_folder = BASE_DIR + "settings_config/" + cur_garment_folder + '/pattern/'
    from utils_main import identify_selected_edges,add_bridges,obj_activate
    error_found,select_vgroups = identify_selected_edges()

    if error_found or len(select_vgroups) != 2:
        ret_str = "Error found , selected_edges = " + str(select_vgroups)
        raise ValueError(ret_str)

    
    spring_template = {
            'type' : 'edge_edge',
            'vertex_group1' : '',
            'vertex_group2' : '',
            'reverse' : internal_reverse,
            'add' : internal_add,
    }


    ob = obj_activate('default')
    spring_template['vertex_group1'] = select_vgroups[0] 
    spring_template['vertex_group2'] = select_vgroups[1] 

    def get_spring_name(springs_dict):
        cnt = 0
        spring_new_found = False
        spring_new_name = ''
        while not spring_new_found:
            if 'spring'+str(cnt) not in springs_dict.keys():
                spring_new_found = True
                spring_new_name = 'spring' + str(cnt)
            if cnt > 100:
                spring_new_found = True

            cnt+=1

        return spring_new_name

    def matched_count(spring_template,current_spring_dict):
        f1 = spring_template['vertex_group1']
        f2 = spring_template['vertex_group2']
        both_matched_count = 0
        for cur_spring_name in current_spring_dict:
            cur_spring = current_spring_dict[cur_spring_name]
            l1 = cur_spring['vertex_group1']
            l2 = cur_spring['vertex_group2']

            if f1 == l1 and f2 == l2:
                both_matched_count+=1
            elif f1 == l2 and f2 == l1:
                both_matched_count+=1

        return both_matched_count


    from zodbpickle import pickle
    import copy
    current_spring_dict = {}
    if os.path.exists(ob['springs_pickle_path']):
        current_spring_dict = pickle.load(open(ob['springs_pickle_path'],'rb'))
    both_matched_count = matched_count(spring_template,current_spring_dict)

    if both_matched_count == 0:
        new_spring = get_spring_name(current_spring_dict)
        current_spring_dict[new_spring] = copy.copy(spring_template)
        add_one_spring(ob,spring_template)
        
    else:
        ret_str = "spring exists already"
        raise ValueError(ret_str)


    pickle.dump(current_spring_dict,open(ob['springs_pickle_path'],'wb'))

    bpy.ops.object.mode_set(mode="EDIT")
    bpy.ops.mesh.select_mode(type="EDGE")
    bpy.ops.mesh.select_all(action='DESELECT')


class AddSpring(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.add_spring"
    bl_label = "GAZER ADD SPRING"
    bl_options = {'REGISTER', 'UNDO'}  

    internal_reverse = bpy.props.IntProperty(default=-1)
    internal_add = bpy.props.BoolProperty(default=True)

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        main_add_spring(context,self.internal_reverse,self.internal_add)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(AddSpring)

def unregister():
    bpy.utils.unregister_class(AddSpring)

if __name__ == "__main__":
    register()
    # # test call
    # bpy.ops.gazer.add_spring()
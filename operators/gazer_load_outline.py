def main_load_outline(context):
    import bpy
    from imp import reload
    import utils_main
    reload(utils_main)

    from utils_main import obj_activate,hide_rest,clear_data
    # file_path = bpy.data.filepath
    import os,sys,inspect
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    cur_garment_folder = bpy.context.scene.garment_folder
    cur_pattern_folder = bpy.context.scene.pattern_folder

    settings_config_folder = BASE_DIR + "settings_config/" + cur_garment_folder + '/pattern/' + cur_pattern_folder + '/'
    outline_blend_file = settings_config_folder + 'outline.blend'

    # from utils_main import os_blend_file
    
    # os_blend_file(outline_blend_file,bpy.data.filepath)
    from utils_main import obj_activate
    if cur_pattern_folder in bpy.data.objects.keys():
        ob = obj_activate(cur_pattern_folder)
        bpy.ops.object.delete(use_global=False)

    blend_import_path = outline_blend_file +"/Object/" #TODO : Id no .obj file , just draw the pattern instead
    bpy.ops.wm.append(directory=blend_import_path,filename=cur_pattern_folder)

    
    obj_activate(cur_pattern_folder)





    
class LoadOutline(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.load_outline"
    bl_label = "GAZER LOAD OUTLINE"
    bl_options = {'REGISTER', 'UNDO'}  

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        main_load_outline(context)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(LoadOutline)

def unregister():
    bpy.utils.unregister_class(LoadOutline)

if __name__ == "__main__":
    register()
    # # test call
    # bpy.ops.gazer.load_outline()
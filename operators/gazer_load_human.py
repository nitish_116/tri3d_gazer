def main_load_human(context):
    import bpy
    from imp import reload
    import utils_main
    reload(utils_main)

    from utils_main import obj_activate,hide_rest,clear_data
    # file_path = bpy.data.filepath
    import os,sys,inspect
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()

    keyword_humans = 'humans'
    human_name = 'BASTIONI'    
    folder_name = bpy.context.scene.human_folder
    blend_file_name = 'avt_0.blend'
    
    clear_data()
    human_blend_file_path = BASE_DIR + keyword_humans +'/'+folder_name+'/'+blend_file_name
    blend_import_path = human_blend_file_path+"/Object/" #TODO : Id no .obj file , just draw the pattern instead
    bpy.ops.wm.append(directory=blend_import_path,filename=human_name)

    obj_activate(human_name)
    hide_rest(human_name)

    for x in range(45):
        bpy.context.object.active_shape_key_index = 0
        bpy.ops.object.shape_key_remove()

    # bpy.context.object.active_shape_key_index = 0
    # bpy.ops.object.shape_key_remove()
    bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Armature")


    bpy.ops.transform.rotate(value=-1.5708, axis=(1, 0, 0), constraint_axis=(True, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)

class LoadHuman(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.load_human"
    bl_label = "GAZER LOAD HUMAN"
    bl_options = {'REGISTER', 'UNDO'}  

    

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        main_load_human(context)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(LoadHuman)

def unregister():
    bpy.utils.unregister_class(LoadHuman)

if __name__ == "__main__":
    register()
    # # test call
    # bpy.ops.gazer.load_human()
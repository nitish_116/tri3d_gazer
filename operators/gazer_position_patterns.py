def main_position_patterns(context):
    import bpy
    from imp import reload
    
    # file_path = bpy.data.filepath
    import os,sys,inspect
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()

    import utils_main,utils_pattern,utils_garment
    reload(utils_main)
    reload(utils_pattern)
    reload(utils_garment)

    from utils_main import obj_activate,hide_rest,clear_data

    cur_garment_folder = bpy.context.scene.garment_folder
    cur_pattern_folder = bpy.context.scene.pattern_folder

    garment_config_folder = BASE_DIR + "settings_config/" + cur_garment_folder + '/garment/default/'
    pattern_config_folder = BASE_DIR + "settings_config/" + cur_garment_folder + '/pattern/'
    positions_blend_file = garment_config_folder + 'positions.blend'

    for ob in bpy.data.objects:
        if 'pattern' in ob.name:
            ob.select = True
        else:
            ob.select = False
    bpy.ops.object.delete(use_global=False)

    
    for cur_pattern in os.listdir(pattern_config_folder):
        if 'default' != cur_pattern and 'pattern' in cur_pattern:
            blend_import_path = positions_blend_file+"/Object/" #TODO : Id no .obj file , just draw the pattern instead
            bpy.ops.wm.append(directory=blend_import_path,filename=cur_pattern)

    for ob in bpy.data.objects:
        if 'pattern' in ob.name and ob['gazer_type'] == 'pattern':
            ob.select = True
            bpy.context.scene.objects.active = ob
        else:
            ob.select = False


    # raise ValueError()
    
    bpy.ops.object.join()
    garment_name = 'default'
    bpy.context.active_object.name = garment_name
    from utils_main import obj_activate
    ob = obj_activate(garment_name)
    ob['springs_pickle_path'] = garment_config_folder + 'springs.pickle'
    ob['springs_txt_path'] = garment_config_folder + 'springs.txt'

    from utils_garment import add_all_springs
    if os.path.exists(ob['springs_pickle_path']):
        add_all_springs(ob)
    


    bpy.ops.object.mode_set(mode="EDIT")
    bpy.ops.mesh.select_mode(type="EDGE")
    bpy.ops.mesh.select_all(action='DESELECT')


class PositionPatterns(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.position_patterns"
    bl_label = "GAZER POSITION PATTERNS"
    bl_options = {'REGISTER', 'UNDO'}  

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        main_position_patterns(context)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(PositionPatterns)

def unregister():
    bpy.utils.unregister_class(PositionPatterns)

if __name__ == "__main__":
    register()
    # # test call
    # bpy.ops.gazer.position_patterns()
def sys_path_additions():
    import os,sys,inspect
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    cur_dir_split = CUR_DIR.split("/") 
    BASE_DIR  = "/".join(cur_dir_split[0:-1]) + "/"
    UTILS_DIR = BASE_DIR + 'utils/'
    if CUR_DIR not in sys.path:
    	sys.path.append(CUR_DIR)
    if UTILS_DIR not in sys.path:
        sys.path.append(UTILS_DIR)
    return BASE_DIR


    import utils_custom_object,utils_garment,utils_main,utils_path_definitions,utils_pattern
    reload(utils_custom_object)
    reload(utils_garment)
    reload(utils_main)
    reload(utils_path_definitions)
    reload(utils_pattern)





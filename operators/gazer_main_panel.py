import bpy

class gazer_main_panel(bpy.types.Panel):
    """Creates a Panel in the Object properties window"""
    bl_label = "gazer_main_panel"
    bl_idname = "OBJECT_PT_gazer_main_panel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = "gazer"

    def draw(self, context):
        layout = self.layout

        layout.label('Human')
        layout.prop(context.scene, "human_folder")
        row = layout.row()
        row.operator("gazer.load_human",text="Load Human")

        layout.label('Garment')
        layout.prop(context.scene, "garment_folder")
        row = layout.row()
        row.operator("gazer.create_garment",text="Create Garment")

        layout.prop(context.scene, "pattern_folder")

        row = layout.row()
        row.operator("gazer.new_pattern",text="New Pattern")

        row = layout.row()
        row.operator("gazer.load_outline",text="Load Outline")

        row = layout.row()
        row.operator("gazer.update_pattern",text="Update Pattern")

        row = layout.row()
        row.operator("gazer.load_all_patterns",text="Load All Patterns")

        row = layout.row()
        row.operator("gazer.position_patterns",text="Position Patterns")        

        row = layout.row()
        row.operator("gazer.save_pattern_positions",text="Save Pattern Positions")

        row = layout.row()
        row.operator("gazer.add_spring",text="Add Spring")

        row = layout.row()
        row.operator("gazer.delete_spring",text="Delete Spring")

        row = layout.row()
        row.operator("gazer.delete_springs",text="Delete All Springs")
        
        row = layout.row()
        row.operator("gazer.mesh_filler",text="Fill Mesh")

        row = layout.row()
        row.operator("gazer.position_meshed_patterns",text="Prepare Garment")

def items_cb_human(self, context):
    mats = [] 
    import os,sys,inspect
    from imp import reload
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    dir_name = BASE_DIR + 'humans/'

    for cur_folder in os.listdir(dir_name):
        if 'DS_Store' not in cur_folder:#'pre_prod' in cur_folder:
            mats.append((cur_folder,cur_folder,cur_folder)) # id, label, description
    return mats

def items_cb_garment(self, context):
    mats = [] 
    import os,sys,inspect
    from imp import reload
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    dir_name = BASE_DIR + 'settings_config/'


    for cur_folder in os.listdir(dir_name):
        if 'DS_Store' not in cur_folder:#'pre_prod' in cur_folder:
            mats.append((cur_folder,cur_folder,cur_folder)) # id, label, description
    return mats


def items_cb_pattern(self, context):
    mats = [] 
    import os,sys,inspect
    from imp import reload
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()

    dir_name = BASE_DIR + 'settings_config/'+str(bpy.context.scene.garment_folder)+'/pattern/'
    if os.path.exists(dir_name):
        for cur_folder in os.listdir(dir_name):
            if 'DS_Store' not in cur_folder:#'pre_prod' in cur_folder:
                mats.append((cur_folder,cur_folder,cur_folder)) # id, label, description
    return mats

def register():
    bpy.types.Scene.human_folder = \
    bpy.props.EnumProperty(items=items_cb_human)

    bpy.types.Scene.garment_folder = \
    bpy.props.EnumProperty(items=items_cb_garment)

    bpy.types.Scene.pattern_folder = \
    bpy.props.EnumProperty(items=items_cb_pattern)

    #Put all these at a place to edit them
    bpy.types.Scene.basic_unit = bpy.props.FloatProperty(default=1.0)
    bpy.types.Scene.max_volume = bpy.props.FloatProperty(default=0.5)

    bpy.utils.register_class(gazer_main_panel)

def unregister():
    bpy.utils.unregister_class(gazer_main_panel)

if __name__ == "__main__":
    register()
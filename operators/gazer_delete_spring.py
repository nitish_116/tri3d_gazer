def main_delete_spring(context):
    import bpy
    from imp import reload
    import utils_main,utils_pattern,utils_garment
    reload(utils_main)
    reload(utils_pattern)
    reload(utils_garment)

    from utils_main import obj_activate,hide_rest,clear_data
    # file_path = bpy.data.filepath
    import os,sys,inspect
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    cur_garment_folder = bpy.context.scene.garment_folder
    cur_pattern_folder = bpy.context.scene.pattern_folder

    garment_config_folder = BASE_DIR + "settings_config/" + cur_garment_folder + '/garment/default/'
    pattern_config_folder = BASE_DIR + "settings_config/" + cur_garment_folder + '/pattern/'
    
    
    from zodbpickle import pickle
    import copy
    from utils_main import obj_activate
    from pprint import pprint
    ob = obj_activate('default')
    current_spring_dict = {}
    if os.path.exists(ob['springs_pickle_path']):
        current_spring_dict = pickle.load(open(ob['springs_pickle_path'],'rb'))
    else:
        raise ValueError("file path does not exist")
    


    from utils_main import identify_selected_springs
    from utils_garment import identify_spring,delete_spring_visually
    intersection_springs = identify_selected_springs(current_spring_dict)


    if len(intersection_springs) != 1:
        ret_str = "Select the spring correctly for deleting " + str(intersection_springs)
        raise ValueError(ret_str)

    
    del current_spring_dict[intersection_springs[0]]
    pickle.dump(current_spring_dict,open(ob['springs_pickle_path'],'wb'))

    bpy.ops.object.mode_set(mode="OBJECT")
    bpy.ops.gazer.load_all_patterns()
    bpy.ops.gazer.position_patterns()

    
    # pprint(matched_springs)

    

class DeleteSpring(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.delete_spring"
    bl_label = "GAZER DELETE SPRING"
    bl_options = {'REGISTER', 'UNDO'}  

    

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        main_delete_spring(context)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(DeleteSpring)

def unregister():
    bpy.utils.unregister_class(DeleteSpring)

if __name__ == "__main__":
    register()
    # # test call
    # bpy.ops.gazer.delete_spring()
def main_load_mesh(context):
    import os,sys,inspect,importlib
    from imp import reload
    import bpy,copy
    from pprint import pprint
    from zodbpickle import pickle

    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    UTILS_DIR = BASE_DIR + 'utils/'
    for cur_util_py in os.listdir(UTILS_DIR):
        if 'utils' in cur_util_py and '.py' in cur_util_py:
            cur_util  = cur_util_py.split(".")[0]
            try:
                class_module = importlib.import_module(cur_util)
                reload(class_module)
            except ImportError:
                pass

    bpy.ops.gazer.load_human()
    bpy.ops.transform.rotate(value=1.5708, axis=(1, 0, 0), constraint_axis=(True, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)

    from utils_path_definitions import get_dynamic_properties,get_pattern_list
    from utils_main import obj_activate
    cnt = 0
    for cur_pattern in get_pattern_list(BASE_DIR):
        if 1:#cnt == 0:
            bpy.context.scene.pattern_folder = cur_pattern
            dp = get_dynamic_properties(BASE_DIR)

            if cur_pattern in bpy.data.objects.keys():
                ob = obj_activate(cur_pattern)
                bpy.ops.object.delete(use_global=False)

            blend_import_path = dp['mesh_blend_path'] +"/Object/" #TODO : Id no .obj file , just draw the pattern instead
            print(blend_import_path,cur_pattern)
            bpy.ops.wm.append(directory=blend_import_path,filename=cur_pattern)
        cnt+=1

        
        





    
class LoadMesh(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.load_mesh"
    bl_label = "GAZER LOAD MESH"
    bl_options = {'REGISTER', 'UNDO'}  

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        main_load_mesh(context)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(LoadMesh)

def unregister():
    bpy.utils.unregister_class(LoadMesh)

if __name__ == "__main__":
    register()
    # # test call
    # bpy.ops.gazer.load_pattern()
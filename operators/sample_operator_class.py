def main_sample_operator(context):
    
    pass

class SampleOperator(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.sample_operator"
    bl_label = "GAZER SAMPLE OPERATOR"
    bl_options = {'REGISTER', 'UNDO'}  

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        main_sample_operator(context)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(SampleOperator)

def unregister():
    bpy.utils.unregister_class(SampleOperator)

if __name__ == "__main__":
    register()
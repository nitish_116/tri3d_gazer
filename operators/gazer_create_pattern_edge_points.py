def ordered_edge_list(edge_list):
    ans = []
    for edge in edge_list:
        v1 = edge[0]
        v2 = edge[1]
        if v1 < v2:
            ans.append((v1,v2))
        elif v1 > v2:
            ans.append((v2,v1))

    ans = list(set(ans))
    return ans            


def generate_edge_list2(face_list,order=False):
    edge_list = []

    for face in face_list:
        for ii in range(len(face)):
            if ii < len(face)-1:
                edge_list.append((face[ii],face[ii+1]))
            if ii == len(face)-1:
                edge_list.append((face[ii],face[0]))   
                 
    if order:
        edge_list = ordered_edge_list(edge_list)                        
    return edge_list            


def add_vertex_groups(cur_name,mapped_corner_vertices_dict,mapped_edge_vertices_dict):

    # def closest_vertex(co,object_name):
    #     def dist(p1, p2):
    #         import math
    #         return math.sqrt((p2[0] - p1[0]) ** 2 + (p2[1] - p1[1]) ** 2 + (p2[2] - p1[2]) ** 2)

    #     from blender_utils_old import obj_activate
    #     ob = obj_activate(object_name)
    #     dist_min = 100000
    #     ind_min = -1
    #     for vert in ob.data.vertices:
    #         current_dist = dist(tuple(vert.co),co)
    #         if current_dist <= dist_min:
    #             ind_min = vert.index
    #             dist_min = current_dist
    #     return ind_min,dist_min
    from utils_main import obj_activate,objs_deactivate
    objs_deactivate()
    
    ob = obj_activate(cur_name)
    for corner_grp_name in mapped_corner_vertices_dict:
        ob.vertex_groups.new(corner_grp_name)
        ob.vertex_groups[corner_grp_name].add(mapped_corner_vertices_dict[corner_grp_name],1,'REPLACE')
    bpy.ops.object.mode_set(mode="OBJECT")
    
    bpy.ops.object.mode_set(mode="OBJECT")                
    for cur_edge_name in mapped_edge_vertices_dict:
        ob.vertex_groups.new(cur_edge_name)
        ob.vertex_groups[cur_edge_name].add(mapped_edge_vertices_dict[cur_edge_name],1,'REPLACE')
    bpy.ops.object.mode_set(mode="OBJECT")


    all_vertices = range(len(ob.data.vertices))
    g_name = cur_name + '_all'
    ob.vertex_groups.new(g_name)
    ob.vertex_groups[g_name].add(all_vertices,1,'REPLACE')


def external_triangulate():
    from pprint import pprint
    ob = bpy.context.active_object


    bpy.ops.object.mode_set(mode="EDIT")
    import bmesh
    me = bpy.context.scene.objects.active.data
    bm = bmesh.from_edit_mesh(me)

    triangulated_list = {}
    for face in bm.faces:
        for vert in face.verts:
            if face.index in triangulated_list.keys():
                triangulated_list[face.index].append(tuple(vert.co))
            else:
                triangulated_list[face.index] = [tuple(vert.co)]
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode="OBJECT")



    def delete_object(object_name):
        from utils_main import objs_deactivate
        objs_deactivate()
        if object_name in bpy.data.objects.keys():
            bpy.data.objects[object_name].select = True
        bpy.ops.object.delete(use_global=False)

    delete_object(ob.name)


    def meshpy_triangulate(vert_list,face_index):



        import meshpy
        from imp import reload
        reload(meshpy)
        from meshpy import triangle
        import numpy as np

        vert_list_2d = []
        for vert in vert_list:
            vert_2d = (vert[0],vert[1])
            vert_list_2d.append(vert_2d)
        
        
        edge_list = generate_edge_list2([list(range(len(vert_list_2d)))])

        info = triangle.MeshInfo()
        info.set_points(vert_list_2d)
        info.set_facets(edge_list)
        from pprint import pprint


        mesh = triangle.build(info, verbose=False,allow_volume_steiner=False,allow_boundary_steiner=False,max_volume=0.005,quality_meshing=True,generate_faces=True,generate_edges=True)
        
        B = {}
        B['vertices'] = []
        for point in mesh.points:
            B['vertices'].append(tuple(point))

        B['edges'] = []
        
        for face in mesh.faces:
            B['edges'].append(face)

        B['faces'] = []
        for facet in mesh.elements:
            B['faces'].append(facet)

        
        def draw_pattern_edges(name,coord_list,edge_list,t_face_list,reverse=False):

            me = bpy.data.meshes.new(name)
            ob = bpy.data.objects.new(name, me)
            scn = bpy.context.scene
            scn.objects.link(ob)
            scn.objects.active = ob
            me.from_pydata(coord_list,[],t_face_list)

        t_vert_list_3d = []
        t_edge_list = []
        t_face_list = []
        for vert in B['vertices']:
            t_vert_list_3d.append((vert[0],vert[1],0))
        for t in B['edges']:
            t_edge_list.append([int(t[0]),int(t[1])])   
        for t in B['faces']:
            t_face_list.append([int(t[0]),int(t[1]),int(t[2])])   

        
        cur_pattern = bpy.context.scene.pattern_folder
        draw_pattern_edges(cur_pattern,t_vert_list_3d,t_edge_list,t_face_list,reverse=True)
        
        bpy.ops.object.mode_set(mode="EDIT")
        # bpy.ops.mesh.select_all(action='SELECT')
        # bpy.ops.mesh.edge_face_add()
                                
        bpy.ops.mesh.select_all(action='DESELECT')
        me = bpy.context.scene.objects.active.data
        bm = bmesh.from_edit_mesh(me)
        cnt = 0

        return 0


        for face in bm.faces:
            if len(face.verts) != 3:
                face.select = True
                cnt+=1

            else:
                face.select = False
        

        #new triangles
        bpy.ops.mesh.delete(type='ONLY_FACE')

        

        if self.reverse:
            bpy.ops.mesh.select_all(action='SELECT')
            bpy.ops.mesh.flip_normals()

        bpy.ops.object.mode_set(mode="OBJECT")


    cnt = 0
    for face_index in triangulated_list:
        if 1:#cnt == 0:
            # uniform_triangulate(triangulated_list[face_index],face_index)
            meshpy_triangulate(triangulated_list[face_index],face_index)
        cnt+=1

    bpy.ops.object.mode_set(mode="EDIT")
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.remove_doubles()
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode="OBJECT")


#stores all the internal edge points as a list for the (starting_point, ending_point)
def identify_edge_points(starting_list, final_list):
    cnt = 0
    ans = {}
    for tmpx in starting_list:
        cur_starting_point = starting_list[cnt]
        cur_starting_index = -1
        if cur_starting_point in final_list:
            cur_starting_index = final_list.index(cur_starting_point)

        if cnt < len(starting_list) -1:
            cur_ending_point = starting_list[cnt+1]
            cur_ending_index = -1
            if cur_ending_point in final_list:
                cur_ending_index = final_list.index(cur_ending_point)
        elif cnt == len(starting_list) - 1:
            cur_ending_point = starting_list[0]
            cur_ending_index = len(final_list)
        
        tmp_list = []
        if cur_starting_index >= 0  and cur_ending_index >= 0:
            min_index = min(cur_starting_index,cur_ending_index)
            max_index = max(cur_starting_index,cur_ending_index)
            tmp_list = final_list[min_index+1:max_index]
            error_found = False
            for cur_elem in tmp_list:
                if cur_elem in starting_list:
                    error_found = True
            if error_found:
                tmp_list = []
        edge_def = (cur_starting_point,cur_ending_point)
        ans[edge_def] = [cur_starting_point] + tmp_list + [cur_ending_point]
        cnt+=1

    return ans

def draw_pattern(name,coord_list,face_list,reverse=False):

      me = bpy.data.meshes.new(name)
      ob = bpy.data.objects.new(name, me)
      scn = bpy.context.scene
      scn.objects.link(ob)
      scn.objects.active = ob
      reversed_face_list = []
      if reverse:
        for face in face_list:
            reversed_face_list.append(face.reverse())
      
      me.from_pydata(coord_list,[],face_list)
      
def rounding_tuple(a,val):
    b = []
    for elem in a:
        b.append(round(float(elem),val))
    return tuple(b)

def read_initial_coords():
    cur_pattern = bpy.context.scene.pattern_folder
    from utils_main import obj_activate,objs_deactivate
    objs_deactivate()
    ob = obj_activate(cur_pattern)
    mat = ob.matrix_world
    initial_coords = {}
    for vert in ob.data.vertices:
        initial_coords[vert.index] = tuple(mat*vert.co)
        # initial_coords[vert.index] = rounding_tuple(tuple(mat*vert.co),1)

    return initial_coords

def return_ordered_edges(ic,edge_points_dict):
    import collections
    sorted_edge_points_dict ={}
    sorted_edge_points_dict = collections.OrderedDict(sorted_edge_points_dict)

    vertices_count = len(ic)
    
    for cur_vertex in range(vertices_count):
        if cur_vertex == vertices_count -1:
            cur_edge = (0,cur_vertex)
        else:
            cur_edge = (cur_vertex,cur_vertex+1)
        
        sorted_edge_points_dict[cur_edge] = edge_points_dict[cur_edge]

    return sorted_edge_points_dict

def update_edge_lines(edge_coords_dict,vertex_map,cur_edge,cur_number_points,final_number_points,ic):
    from pprint import pprint
    insert_index = vertex_map.index(cur_edge[0])
    next_index = vertex_map.index(cur_edge[1])

    #TODO : Make Sure next_index - insert_index == 1 modulo length
    edge_case = False
    if next_index - insert_index != 1:
        edge_case = True
    


    from sympy import Line3D,Symbol
    from sympy.geometry.point import Point3D
    from sympy.geometry import Point

    def subs_point(L,val):
        t=Symbol('t',real=True)
        ap = L.arbitrary_point()
        return Point3D(ap.x.subs(t,val),ap.y.subs(t,val),ap.z.subs(t,val))

    sp = 0
    ep = 1
    if edge_case:
        sp = 1
        ep = 0

    P1 = Point3D(ic[cur_edge[sp]])
    P2 = Point3D(ic[cur_edge[ep]])
    L = Line3D(P1,P2)

    new_points = list(range(cur_number_points,final_number_points))
    if edge_case:
        vertex_map.extend(new_points)
    else:
        vertex_map[next_index:1] = new_points

    lnx = float(len(new_points))
    cnt = 0
    for new_point in new_points:
        t_value = float((cnt+1)/(lnx+1))
        edge_coords_dict[new_point] = tuple(subs_point(L,t_value))
        cnt+=1

    return edge_coords_dict,vertex_map

def main_create_pattern_edge_points(context):
    import os,sys,inspect,importlib
    from imp import reload
    import bpy,copy
    from pprint import pprint
    from zodbpickle import pickle

    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    UTILS_DIR = BASE_DIR + 'utils/'
    for cur_util_py in os.listdir(UTILS_DIR):
        if 'utils' in cur_util_py and '.py' in cur_util_py:
            cur_util  = cur_util_py.split(".")[0]
            try:
                class_module = importlib.import_module(cur_util)
                reload(class_module)
            except ImportError:
                pass

    from utils_main import clear_data 
    cur_pattern = bpy.context.scene.pattern_folder
    # bpy.ops.gazer.adjust_edge_points()

    clear_data()
    current_file_path = bpy.data.filepath
    bpy.ops.wm.save_mainfile()    
    from utils_main import os_blend_file
    os_blend_file(current_file_path,current_file_path)

    bpy.ops.gazer.load_outline()
    

    edge_points_dict = {}
    from utils_path_definitions import get_dynamic_properties
    dp = get_dynamic_properties(BASE_DIR)
    if os.path.exists(dp['edge_points_pickle_path']):
        edge_points_dict = pickle.load(open(dp['edge_points_pickle_path'],'rb'))

    
    if cur_pattern not in edge_points_dict.keys():
        ret_str = cur_pattern + " not found in edge_points_dict"
        raise ValueError(ret_str)

    #create new edge points and the vertex groups of the edges need to be updated
    ic = read_initial_coords()
    sorted_edge_points_dict = return_ordered_edges(ic,edge_points_dict[cur_pattern])
    vertex_map = list(ic.keys())
    

    edge_coords_dict = copy.copy(ic)
    for cur_edge in sorted_edge_points_dict:
        cur_number_points = len(edge_coords_dict)
        final_number_points = cur_number_points + edge_points_dict[cur_pattern][cur_edge]
        edge_coords_dict,vertex_map = update_edge_lines(edge_coords_dict,vertex_map,cur_edge,cur_number_points,final_number_points,ic)
        
    

    clear_data()
    
    draw_pattern(cur_pattern,list(edge_coords_dict.values()),[vertex_map],reverse=False)

    

    from utils_main import obj_activate,objs_deactivate
    objs_deactivate()
    ob = obj_activate(cur_pattern)
    
    point_to_edge_mapping = identify_edge_points(list(ic.keys()),vertex_map)

    mapped_edge_vertices_dict = {}
    for edge_name in point_to_edge_mapping:
        min_vert = min(edge_name[0],edge_name[1])
        max_vert = max(edge_name[0],edge_name[1])
        edge_grp_name = cur_pattern + "_edge_" + str(min_vert) + "_" + str(max_vert)
        mapped_edge_vertices_dict[edge_grp_name] = []
        for cur_vert in point_to_edge_mapping[edge_name]:
            vert_index = vertex_map.index(cur_vert)
            if vert_index >= 0 and vert_index < len(vertex_map):
                mapped_edge_vertices_dict[edge_grp_name].append(vert_index)
            else:
                ret_str = "vert_index not found for cur_vert == " + str(cur_vert) + " in self.enhanced_face_vertex_map[0] " + str(self.enhanced_face_vertex_map)
                raise ValueError(ret_str)
    

    mapped_corner_vertices_dict = {}
    for cur_corner in list(ic.keys()):
        corner_grp_name = cur_pattern + '_corner_' + str(cur_corner)
        corner_index = vertex_map.index(cur_corner)
        mapped_corner_vertices_dict[corner_grp_name] = []
        if corner_index >= 0 and corner_index <  len(vertex_map):
            mapped_corner_vertices_dict[corner_grp_name].append(corner_index)
        else:
            ret_str = "vert_index not found for cur_vert == " + str(cur_vert) + " in self.enhanced_face_vertex_map[0] " + str(self.enhanced_face_vertex_map)
            raise ValueError(ret_str)
    

    external_triangulate()
    objs_deactivate()
    ob = obj_activate(cur_pattern)
    add_vertex_groups(ob.name,mapped_corner_vertices_dict,mapped_edge_vertices_dict)

    cur_garment_folder = bpy.context.scene.garment_folder
    cur_pattern_folder = bpy.context.scene.pattern_folder

    settings_config_folder = BASE_DIR + "settings_config/" + cur_garment_folder + '/pattern/' + cur_pattern_folder + '/'
    mesh_blend_path = settings_config_folder + 'mesh.blend'
    current_file_path = bpy.data.filepath
    bpy.ops.wm.save_mainfile()    
    from utils_main import os_blend_file
    os_blend_file(current_file_path,mesh_blend_path)

    bpy.ops.wm.open_mainfile(filepath=current_file_path)



class CreatePatternEdgePoints(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.create_pattern_edge_points"
    bl_label = "GAZER CREATE PATTERN EDGE POINTS"
    bl_options = {'REGISTER', 'UNDO'}  

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        main_create_pattern_edge_points(context)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(CreatePatternEdgePoints)

def unregister():
    bpy.utils.unregister_class(CreatePatternEdgePoints)

if __name__ == "__main__":
    register()
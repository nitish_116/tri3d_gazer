def create_position_pickles(garment_config_folder):
    from pprint import pprint
    for ob in bpy.data.objects:
        ob.select = False

    for ob in bpy.data.objects:
        ob.select = True
        bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY')
        ob.select=False

    from zodbpickle import pickle

    old_position_dict = {}
    pickle_path = garment_config_folder + 'positions.pickle'
    txt_path = garment_config_folder + 'positions.txt'
    
    if os.path.exists(pickle_path):
        print(pickle_path)
        old_position_dict = pickle.load(open(pickle_path,'rb'))

    import copy
    pr_dict = copy.copy(old_position_dict)
    
    for ob in bpy.data.objects:
        if 'pattern' in ob.name:
            # bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)
            mat = ob.matrix_world
            pr_dict[ob.name] = {}
            pr_dict[ob.name]['location'] = list(ob.location)
            pr_dict[ob.name]['rotation'] = list(ob.rotation_euler)
            pr_dict[ob.name]['location_0'] = list(mat*ob.data.vertices[0].co)


    from zodbpickle import pickle
    pickle.dump(pr_dict,open(pickle_path,'wb'))
    with open(txt_path, 'wt') as out:
        pprint(pr_dict, stream=out)


def main_save_pattern_positions(context):
    import bpy
    from imp import reload
    import utils_main,utils_pattern
    reload(utils_main)
    reload(utils_pattern)

    from utils_main import obj_activate,hide_rest,clear_data
    # file_path = bpy.data.filepath
    import os,sys,inspect
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    cur_garment_folder = bpy.context.scene.garment_folder
    cur_pattern_folder = bpy.context.scene.pattern_folder

    garment_config_folder = BASE_DIR + "settings_config/" + cur_garment_folder + '/garment/default/'
    positions_blend_file = garment_config_folder + 'positions.blend'

    current_file_path = bpy.data.filepath
    
    bpy.ops.wm.save_mainfile() 

    



    from utils_main import os_blend_file
    os_blend_file(current_file_path,positions_blend_file)

    cnt = 0
    for ob in bpy.data.objects:
        if 'pattern' not in ob.name:
            ob.select = True
        else:
            ob.select = False
    bpy.ops.object.delete(use_global=False)

    for ob in bpy.data.objects:
        ob.select = True
    # bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)
    bpy.ops.wm.save_mainfile()

    os_blend_file(current_file_path,current_file_path)

    create_position_pickles(garment_config_folder)




    

    


class SavePatternPositions(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.save_pattern_positions"
    bl_label = "GAZER SAVE PATTERN POSITIONS"
    bl_options = {'REGISTER', 'UNDO'}  

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        main_save_pattern_positions(context)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(SavePatternPositions)

def unregister():
    bpy.utils.unregister_class(SavePatternPositions)

if __name__ == "__main__":
    register()
    # # test call
    # bpy.ops.gazer.save_position_patterns()
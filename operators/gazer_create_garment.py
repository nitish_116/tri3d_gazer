def main_create_garment(context,folder_name):
    import bpy
    from imp import reload
    import utils_main
    reload(utils_main)

    from utils_main import obj_activate,hide_rest,clear_data
    # file_path = bpy.data.filepath
    import os,sys,inspect
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    garment_folder = BASE_DIR + 'settings_config/' + folder_name + '/'

    os.system("mkdir " + garment_folder)
    sample_folder_template = BASE_DIR + 'samples/garment_template/'
    os.system("cp -R "+sample_folder_template+"* " + garment_folder) 

    bpy.context.scene.garment_folder = folder_name



def generate_garment_folder_name():
    
    import os,sys,inspect
    from imp import reload
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    dir_name = BASE_DIR + 'settings_config/'
    name_found = False

    cnt = 0
    cur_garment_name = ''
    while not name_found:
        cur_garment_name = 'garment' + str(cnt)
        if cur_garment_name not in os.listdir(dir_name):
            name_found = True
        cnt+=1
       
    return cur_garment_name


    
class CreateGarment(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.create_garment"
    bl_label = "GAZER CREATE GARMENT"
    bl_options = {'REGISTER', 'UNDO'}  

    g_folder_name = generate_garment_folder_name()
    folder_name = bpy.props.StringProperty(default=g_folder_name) 

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        self.folder_name = generate_garment_folder_name()
        main_create_garment(context,self.folder_name)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(CreateGarment)

def unregister():
    bpy.utils.unregister_class(CreateGarment)

if __name__ == "__main__":
    register()
    # # test call
    # bpy.ops.gazer.create_garment()
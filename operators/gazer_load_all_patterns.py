def main_load_all_patterns(context):
    import bpy
    from imp import reload
    import utils_main
    reload(utils_main)

    from utils_main import obj_activate,hide_rest,clear_data
    # file_path = bpy.data.filepath
    import os,sys,inspect
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    cur_garment_folder = bpy.context.scene.garment_folder
    cur_pattern_folder = bpy.context.scene.pattern_folder

    base_pattern_folder = BASE_DIR + "settings_config/" + cur_garment_folder + '/pattern/'
    from utils_main import clear_data
    clear_data()


    bpy.ops.gazer.load_human()
    bpy.ops.transform.rotate(value=1.5708, axis=(1, 0, 0), constraint_axis=(True, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)

    if os.path.exists(base_pattern_folder):
        for cur_pattern_name in os.listdir(base_pattern_folder):
            if 'default' != cur_pattern_name and 'pattern' in cur_pattern_name:
                print(cur_pattern_name)
                bpy.context.scene.pattern_folder = cur_pattern_name
                bpy.ops.gazer.load_outline()

    # bpy.ops.gazer.position_patterns()


    
class LoadAllPatterns(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.load_all_patterns"
    bl_label = "GAZER LOAD ALL PATTERNS"
    bl_options = {'REGISTER', 'UNDO'}  

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        main_load_all_patterns(context)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(LoadAllPatterns)

def unregister():
    bpy.utils.unregister_class(LoadAllPatterns)

if __name__ == "__main__":
    register()
    # # test call
    # bpy.ops.gazer.load_all_patterns()
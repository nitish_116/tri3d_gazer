def main_delete_springs(context):
    import bpy
    from imp import reload
    import utils_main,utils_pattern
    reload(utils_main)
    reload(utils_pattern)

    from utils_main import obj_activate,hide_rest,clear_data
    # file_path = bpy.data.filepath
    import os,sys,inspect
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    cur_garment_folder = bpy.context.scene.garment_folder
    cur_pattern_folder = bpy.context.scene.pattern_folder

    garment_config_folder = BASE_DIR + "settings_config/" + cur_garment_folder + '/garment/default/'
    pattern_config_folder = BASE_DIR + "settings_config/" + cur_garment_folder + '/pattern/'
    
    
    

    from zodbpickle import pickle
    import copy
    from utils_main import obj_activate
    ob = obj_activate('default')
    current_spring_dict = {}
    pickle.dump(current_spring_dict,open(ob['springs_pickle_path'],'wb'))

    bpy.ops.object.mode_set(mode="OBJECT")
    bpy.ops.gazer.load_all_patterns()
    bpy.ops.gazer.position_patterns()


class DeleteSprings(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.delete_springs"
    bl_label = "GAZER DELETE SPRINGS"
    bl_options = {'REGISTER', 'UNDO'}  

    

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        main_delete_springs(context)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(DeleteSprings)

def unregister():
    bpy.utils.unregister_class(DeleteSprings)

if __name__ == "__main__":
    register()
    # # test call
    # bpy.ops.gazer.delete_springs()
def main_position_meshed_patterns(context):
    import bpy
    from imp import reload
    from zodbpickle import pickle
    
    # file_path = bpy.data.filepath
    import os,sys,inspect
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()

    import utils_main,utils_pattern,utils_garment
    reload(utils_main)
    reload(utils_pattern)
    reload(utils_garment)

    from utils_main import obj_activate,hide_rest,clear_data

    cur_garment_folder = bpy.context.scene.garment_folder
    cur_pattern_folder = bpy.context.scene.pattern_folder

    garment_config_folder = BASE_DIR + "settings_config/" + cur_garment_folder + '/garment/default/'
    pattern_config_folder = BASE_DIR + "settings_config/" + cur_garment_folder + '/pattern/'

    bpy.ops.gazer.load_human()
    bpy.ops.transform.rotate(value=1.5708, axis=(1, 0, 0), constraint_axis=(True, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)
    from utils_main import obj_activate
    from utils_path_definitions import get_dynamic_properties,get_pattern_list
    dp = get_dynamic_properties(BASE_DIR)

    garment_folder_path = dp['garment_folder_path']
    positions_pickle = garment_folder_path + 'positions.pickle'
    positions_dict = pickle.load(open(positions_pickle,'rb'))

    for cur_pattern_folder in get_pattern_list(BASE_DIR):
        if 1:#cnt == 0:
            bpy.context.scene.pattern_folder = cur_pattern_folder
            dp = get_dynamic_properties(BASE_DIR)
            
            if cur_pattern_folder in bpy.data.objects.keys():
                ob = obj_activate(cur_pattern_folder)
                bpy.ops.object.delete(use_global=False)

            blend_import_path = dp['mesh_blend_path'] +"/Object/" #TODO : Id no .obj file , just draw the pattern instead
            bpy.ops.wm.append(directory=blend_import_path,filename=cur_pattern_folder)

            ob = obj_activate(cur_pattern_folder)
            pattern_name = cur_pattern_folder

            if pattern_name in positions_dict.keys():
                if 'rotation' in positions_dict[pattern_name].keys():
                    bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY')
                    sl = positions_dict[pattern_name]['rotation']
                    bpy.context.object.rotation_euler[0] = sl[0]
                    bpy.context.object.rotation_euler[1] = sl[1]
                    bpy.context.object.rotation_euler[2] = sl[2]      

            if pattern_name in positions_dict.keys():
                if 'location' in positions_dict[pattern_name].keys():
                    bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY')
                    sl = positions_dict[pattern_name]['location']
                    bpy.context.object.location[0] = sl[0]
                    bpy.context.object.location[1] = sl[1]
                    bpy.context.object.location[2] = sl[2]      

    for ob in bpy.data.objects:
        if 'pattern' in ob.name:
            ob.select = True
            bpy.context.scene.objects.active = ob
        else:
            ob.select = False
    
    bpy.ops.object.join()
    garment_name = 'default'
    bpy.context.active_object.name = garment_name
    from utils_main import obj_activate
    ob = obj_activate(garment_name)
    ob['springs_pickle_path'] = garment_config_folder + 'springs.pickle'
    ob['springs_txt_path'] = garment_config_folder + 'springs.txt'

    from utils_garment import add_all_springs
    if os.path.exists(ob['springs_pickle_path']):
        add_all_springs(ob)
    
  


class PositionMeshedPatterns(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.position_meshed_patterns"
    bl_label = "GAZER POSITION MESHED PATTERNS"
    bl_options = {'REGISTER', 'UNDO'}  

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        main_position_meshed_patterns(context)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(PositionMeshedPatterns)

def unregister():
    bpy.utils.unregister_class(PositionMeshedPatterns)

if __name__ == "__main__":
    register()
    # # test call
    # bpy.ops.gazer.position_meshed_patterns()
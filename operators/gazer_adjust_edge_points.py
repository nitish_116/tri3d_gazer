def dist(p1, p2):
    import math
    return math.sqrt((p2[0] - p1[0]) ** 2 + (p2[1] - p1[1]) ** 2 + (p2[2] - p1[2]) ** 2)

def generate_edge_list(npoints):
    edge_list = []
    for ii in range(npoints):
        if ii < npoints-1:
            edge_list.append((ii,ii+1))
        if ii == npoints-1:
            edge_list.append((0,ii))
    return edge_list


def fill_default_edge_points(dp):
    import bpy
    from zodbpickle import pickle
    import copy
    bpy.ops.gazer.load_outline()
    
    ob = bpy.context.active_object

    basic_unit = float(ob['basic_unit'])
    basic_unit = 0.5 #hardcoded 

    npoints = len(ob.data.vertices)
    edge_list = generate_edge_list(npoints)

    ans ={}
    for vert in ob.data.vertices:
        ans[vert.index] = tuple(vert.co)
    
    default_edge_points = {}
    for cur_edge in edge_list:
        p1 = ans[cur_edge[0]]
        p2 = ans[cur_edge[1]]
        d12 = dist(p1,p2)

        default_edge_points[cur_edge] = int(d12/basic_unit)-1

    edge_points_dict = {}
    if os.path.exists(dp['edge_points_pickle_path']):
        edge_points_dict = pickle.load(open(dp['edge_points_pickle_path'],'rb'))
    edge_points_dict[bpy.context.scene.pattern_folder] = copy.copy(default_edge_points)
    pickle.dump(edge_points_dict,open(dp['edge_points_pickle_path'],'wb'))

def get_pattern_edge(vgroup_name):
    pname = vgroup_name.split('_')[0]
    e12 = vgroup_name.split('_edge_')[1]
    e1 = int(e12.split('_')[0])
    e2 = int(e12.split('_')[1])

    return (pname,e1,e2)

def adjust_edge_points(springs_dict,edge_points_dict):

    from pprint import pprint
    
    found_bad_springs = True
    iterations_count = 0
    while not found_bad_springs:
        bad_springs_count = 0
        for cur_spring_name in springs_dict:
            cur_spring = springs_dict[cur_spring_name]
            e1_def = get_pattern_edge(cur_spring['vertex_group1'])
            e1_points = edge_points_dict[e1_def[0]][(e1_def[1],e1_def[2])]
            e2_def = get_pattern_edge(cur_spring['vertex_group2'])
            e2_points = edge_points_dict[e2_def[0]][(e2_def[1],e2_def[2])]
            if e1_points != e2_points:
                max_points = max(e1_points,e2_points)
                edge_points_dict[e1_def[0]][(e1_def[1],e1_def[2])] = max_points
                edge_points_dict[e2_def[0]][(e2_def[1],e2_def[2])] = max_points
                bad_springs_count += 1
                print("Adjusting ",str(cur_spring))

        if bad_springs_count > 0:
            found_bad_springs = True
        else:
            found_bad_springs = False
        iterations_count+=1
        if iterations_count > 4:
            ret_str = "More than " + str(iterations_count) + "iterations!"
            raise ValueError(ret_str)

    return edge_points_dict


def main_adjust_edge_points(context):
    import os,sys,inspect,importlib
    from imp import reload
    import bpy,copy
    from pprint import pprint
    from zodbpickle import pickle

    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    if CUR_DIR not in sys.path:
        sys.path.append(CUR_DIR)
    import utils_loader
    reload(utils_loader)
    from utils_loader import sys_path_additions
    BASE_DIR = sys_path_additions()
    UTILS_DIR = BASE_DIR + 'utils/'
    for cur_util_py in os.listdir(UTILS_DIR):
        if 'utils' in cur_util_py and '.py' in cur_util_py:
            cur_util  = cur_util_py.split(".")[0]
            try:
                class_module = importlib.import_module(cur_util)
                reload(class_module)
            except ImportError:
                pass

    from utils_path_definitions import get_dynamic_properties,get_pattern_list
    for cur_pattern in get_pattern_list(BASE_DIR):
        bpy.context.scene.pattern_folder = cur_pattern
        dp = get_dynamic_properties(BASE_DIR)
        #Will be called for each pattern, looped over all the patterns 
        fill_default_edge_points(dp)

    springs_dict = {}
    if os.path.exists(dp['springs_dict_pickle_path']):
        springs_dict = pickle.load(open(dp['springs_dict_pickle_path'],'rb'))

    edge_points_dict = {}
    if os.path.exists(dp['edge_points_pickle_path']):
        edge_points_dict = pickle.load(open(dp['edge_points_pickle_path'],'rb'))


    adjusted_edge_points_dict = adjust_edge_points(springs_dict,edge_points_dict)
    pickle.dump(adjusted_edge_points_dict,open(dp['edge_points_pickle_path'],'wb'))
    



class AdjustEdgePoints(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "gazer.adjust_edge_points"
    bl_label = "GAZER ADJUST EDGE POINTS"
    bl_options = {'REGISTER', 'UNDO'}  

    @classmethod
    def poll(cls, context):
        return True
    def execute(self, context):
        main_adjust_edge_points(context)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(AdjustEdgePoints)

def unregister():
    bpy.utils.unregister_class(AdjustEdgePoints)

if __name__ == "__main__":
    register()
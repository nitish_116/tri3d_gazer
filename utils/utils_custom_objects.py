def assign_object_type(ob_name,ob_type):
	import bpy
	if ob_name in list(bpy.data.objects.keys()):
		ob = bpy.data.objects[ob_name]
		ob['gazer_type'] = ob_type
	else:
		ret_str = ob_name + " not found in the list"
		raise ValueError(ret_str)

	return ob

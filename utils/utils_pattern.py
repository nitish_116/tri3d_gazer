def rounding_tuple(a,val):
    b = []
    for elem in a:
        b.append(round(float(elem),val))
    return tuple(b)

def check_for_correct_type(ob,gtype):
	ans = True
	if 'gazer_type' not in ob.keys():
		ans = False

		if ob['gazer_type'] != gtype:
			ans = False

	return ans


def assign_corner_vertex_groups(ob):
	if not check_for_correct_type(ob,'pattern'):
		raise ValueError("Not the correct type")

	for vert in ob.data.vertices:
		cur_corner_name = ob.name + '_corner_' + str(vert.index)
		vg = ob.vertex_groups.get(cur_corner_name)
		if vg is None:
			ob.vertex_groups.new(cur_corner_name)
		ob.vertex_groups[cur_corner_name].add([vert.index],1,'REPLACE')

	return ob
def assign_edge_vertex_groups(ob):
	if not check_for_correct_type(ob,'pattern'):
		raise ValueError("Not the correct type")
	for edge in ob.data.edges:
		vert_list = []
		for vert in edge.vertices:
			vert_list.append(vert)
		min_index = min(vert_list)
		max_index = max(vert_list)
		cur_edge_name = ob.name + '_edge_' + str(min_index) + '_' + str(max_index)
		vg = ob.vertex_groups.get(cur_edge_name)
		if vg is None:
			ob.vertex_groups.new(cur_edge_name)
		ob.vertex_groups[cur_edge_name].add(vert_list,1,'REPLACE')

	return ob








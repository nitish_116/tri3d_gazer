def objs_deactivate():
    import bpy    
    for ob in bpy.data.objects:
        ob.select = False

def obj_activate(object_name):
    import bpy
    objs_deactivate()
    if object_name in bpy.data.objects.keys():
        ob = bpy.data.objects[object_name]
        ob.select = True
        bpy.context.scene.objects.active = ob    
        bpy.ops.object.mode_set(mode="OBJECT")
        return ob
    else:
        return -1

def hide_rest(object_name):
    import bpy    
    for ob in bpy.data.objects:
        if object_name != ob.name:
            ob.hide = True

def clear_data():
    import bpy

    for image in bpy.data.images:
        image.user_clear()
        bpy.data.images.remove(image)

    for armature in bpy.data.armatures:
        armature.user_clear()
        bpy.data.armatures.remove(armature)

    for obj in bpy.data.objects:
        if obj.name in bpy.context.scene.objects.keys():
            bpy.context.scene.objects.unlink(obj)
        obj.user_clear() 
        bpy.data.objects.remove(obj) 

def os_blend_file(start_path, tmp_path,running_from='gui'):
    import bpy
    if running_from == 'gui':
        bpy.context.user_preferences.filepaths.use_load_ui = False
    
    bpy.ops.wm.open_mainfile(filepath=start_path)

    if running_from == 'gui':
        for window in bpy.context.window_manager.windows:
            screen = window.screen
            for area in screen.areas:
                if area.type == 'VIEW_3D':
                    override = {'window': window, 'screen': screen, 'area': area}
                    bpy.ops.screen.screen_full_area(override)  # toggle to maximize
                    bpy.ops.screen.screen_full_area()  # toggle back (must not use overridden context, else it will crash!)
                    havesetthecontext = True
                    break

        bpy.context.user_preferences.system.use_scripts_auto_execute = True
        bpy.context.screen.scene.unit_settings.system = 'IMPERIAL'
        bpy.context.screen.scene.unit_settings.scale_length = 0.0254

    bpy.ops.wm.save_as_mainfile(filepath=tmp_path)

def identify_selected_edges():
    import bpy
    vg_dict = {}
    inv_vg_dict = {}
    for cur_grp in bpy.context.active_object.vertex_groups:
        cur_grp_index = cur_grp.index
        vg_dict[cur_grp.name] = cur_grp_index
        inv_vg_dict[cur_grp_index] = cur_grp.name


    
    error_found = False
    select_vgroups = []
    bpy.ops.object.mode_set(mode="OBJECT")    
    bpy.ops.object.mode_set(mode="EDIT")
    bpy.ops.mesh.select_mode(type="EDGE")
    cnt = 0
    vlist = bpy.context.active_object.data.vertices
    for edge in bpy.context.active_object.data.edges:
        if edge.select:
            cnt+=1
            v1 = vlist[edge.vertices[0]]
            v2 = vlist[edge.vertices[1]]
            g1 = []
            for cur_group in v1.groups:
                g1.append(cur_group.group)
            g2 = []
            for cur_group in v2.groups:
                g2.append(cur_group.group)

            g12 = list(set.intersection(set(g1),set(g2)))
            if len(g12) == 1:
                cur_grp_name = inv_vg_dict[g12[0]]
                select_vgroups.append(cur_grp_name)
            else:
                error_found = True
            
    return error_found,select_vgroups



def identify_selected_springs(springs_dict):
    import bpy
    from pprint import pprint
    import copy
    vg_dict = {}
    inv_vg_dict = {}
    for cur_grp in bpy.context.active_object.vertex_groups:
        cur_grp_index = cur_grp.index
        vg_dict[cur_grp.name] = cur_grp_index
        inv_vg_dict[cur_grp_index] = cur_grp.name

    
    error_found = False
    select_vgroups = []
    bpy.ops.object.mode_set(mode="OBJECT")    
    bpy.ops.object.mode_set(mode="EDIT")
    bpy.ops.mesh.select_mode(type="EDGE")
    cnt = 0
    vlist = bpy.context.active_object.data.vertices

    mega_matched_springs = []

    for edge in bpy.context.active_object.data.edges:
        if edge.select:
            g1 = []
            g2 = []
            cnt+=1
            v1 = vlist[edge.vertices[0]]
            v2 = vlist[edge.vertices[1]] 
            for cur_group in v1.groups:
                if 'edge' in inv_vg_dict[cur_group.group]:
                    g1.append(inv_vg_dict[cur_group.group])
            for cur_group in v2.groups:
                if 'edge' in inv_vg_dict[cur_group.group]:
                    g2.append(inv_vg_dict[cur_group.group])

            matched_springs_count = 0
            matched_springs_dict = {}

            for cur_spring_name in springs_dict:
                cur_spring = springs_dict[cur_spring_name]
                first_group = cur_spring['vertex_group1']
                second_group = cur_spring['vertex_group2']
                if first_group in g1 and second_group in g2:
                    matched_springs_count+=1
                    matched_springs_dict[cur_spring_name] = copy.copy(springs_dict[cur_spring_name])

            mega_matched_springs.append(matched_springs_dict)

    intersection_springs = set(list([]))
    for each_spring in mega_matched_springs:
        cur_spring_list = set(each_spring.keys())
        intersection_springs = set.union(intersection_springs,cur_spring_list)

    for each_spring in mega_matched_springs:
        cur_spring_list = set(each_spring.keys())
        intersection_springs = set.intersection(intersection_springs,cur_spring_list)

    intersection_springs = list(intersection_springs)

    return intersection_springs



    raise ValueError
        
           
    

def select_custom_group_union(ob_name,v_group_list):
    import bpy
    objs_deactivate()
    ob = obj_activate(ob_name)
    bpy.ops.object.mode_set(mode="EDIT")
    bpy.ops.mesh.select_all(action='DESELECT')


    for cur_group in v_group_list:
        cnt = 0
        for vg in ob.vertex_groups:        
            if cur_group == vg.name:
                cnt+=1
                ob.vertex_groups.active_index = vg.index
        if cnt == 1:    

            bpy.ops.object.vertex_group_select()
    


def add_bridges(bridge_vertex_groups,cur_object_name):
    import bpy
    if len(bridge_vertex_groups) > 0:
        #join the springs between duplicated points and the newly imported patterns / bridging
        bpy.ops.object.mode_set(mode="EDIT")
        bpy.ops.mesh.select_all(action='DESELECT')

        print(bridge_vertex_groups)
        select_custom_group_union(cur_object_name,bridge_vertex_groups)
        

        bpy.ops.mesh.bridge_edge_loops()
        bpy.ops.mesh.delete(type='ONLY_FACE')     
        bpy.ops.mesh.select_all(action='DESELECT')

def return_vgroup_indices(ob_name,vg_name):
    import bpy,bmesh
    ob = obj_activate(ob_name) #MISTAKE - there was a random vgroup.new() , copying error

    bpy.ops.object.mode_set(mode="EDIT")
    bpy.ops.mesh.select_all(action='DESELECT')
    cnt = 0
    for vg in ob.vertex_groups:        
        if vg_name == vg.name:
            cnt+=1
            ob.vertex_groups.active_index = vg.index
    if cnt == 1:
        bpy.ops.object.vertex_group_select()        

    if cnt != 1: #MISTAKE - If a vertex_group is not getting selected do not give out the previous indices 
        return []
    #TODO - Search for all the vertex_group_select() and make sure this condition has been implemented

    me = bpy.context.scene.objects.active.data
    bm = bmesh.from_edit_mesh(me)  
    vlist = []
    for vert in bm.verts:
        if vert.select:
            vlist.append(vert.index)

    bpy.ops.mesh.select_all(action='DESELECT')            
    bpy.ops.object.mode_set(mode="OBJECT")

    return vlist

        




 
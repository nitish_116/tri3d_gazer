def add_one_spring(ob,cur_spring):
    import bpy
    import bmesh
    import numpy as np
    from utils_main import add_bridges,return_vgroup_indices
    edge1_list = return_vgroup_indices(ob.name,cur_spring['vertex_group1'])
    edge2_list = return_vgroup_indices(ob.name,cur_spring['vertex_group2'])


    bpy.ops.object.mode_set(mode="EDIT")
    bpy.ops.mesh.select_mode(type="VERT")
    bpy.ops.mesh.select_all(action='DESELECT')
    ans = -1


    if len(edge1_list) != len(edge2_list):
        minlen = min(len(edge1_list),len(edge2_list))
        edge1_list = edge1_list[0:minlen]
        edge2_list = edge2_list[0:minlen]

    
    if len(edge1_list) == len(edge2_list):
        cnt = 0
        for ii in range(len(edge1_list)):
            if 1:#cnt != 0:
                ind1 = edge1_list[ii]
                ind2 = edge2_list[ii]
                me = bpy.context.scene.objects.active.data
                bm = bmesh.from_edit_mesh(me)
                for vert in bm.verts:
                    if vert.index==ind1 or vert.index==ind2:
                        vert.select=True

                bpy.ops.mesh.edge_face_add()
                # cnt_edge = 0
                # for edge in ob.data.edges:
                #     if edge.select:
                #         cnt_edge+=1
                #         edge['gazer_type'] = 'spring'
                #         edge['vertex_group1'] = cur_spring['vertex_group1']
                #         edge['vertex_group2'] = cur_spring['vertex_group2']

                # print(cnt_edge)

                edge_vec = []
                for edge in bm.edges:
                    if edge.select:
                        val = edge.calc_length()
                        edge_vec.append(val)
                        # edge['gazer_type'] = 'spring'
                        # edge['vertex_group1'] = cur_spring['vertex_group1']
                        # edge['vertex_group2'] = cur_spring['vertex_group2']

                edge_array = np.array(edge_vec)
                ans = np.sum(edge_array)
                if 0:
                    edges_select = [f for f in bm.edges if f.select]
                    bmesh.ops.delete(bm, geom=edges_select, context=5)  
                    bmesh.update_edit_mesh(me, True)

                bpy.ops.mesh.select_all(action='DESELECT')
            cnt+=1

def add_all_springs(ob):
    import os,bpy,bmesh
    import numpy as np
    from zodbpickle import pickle
    from pprint import pprint
    from utils_main import add_bridges,return_vgroup_indices
    springs_pickle_path = ob['springs_pickle_path']
    springs_dict = {}
    if os.path.exists(springs_pickle_path):
        springs_dict = pickle.load(open(springs_pickle_path,'rb'))

        for cur_spring_name in springs_dict:
            add_one_spring(ob,springs_dict[cur_spring_name])


def identify_spring(selected_vgroups,springs_dict):
    matches_found = 0
    matched_springs = []
    for cur_spring_name in springs_dict:
        cur_spring = springs_dict[cur_spring_name]
        vg1 = cur_spring['vertex_group1']
        vg2 = cur_spring['vertex_group2']

        if vg1 in selected_vgroups and vg2 in selected_vgroups:
            matches_found+=1
            matched_springs.append(cur_spring_name)


    if matches_found != 1:
        ret_str = "should match exactly once , found " + str(matches_found) + " matches, " + str(matched_springs)
        raise ValueError(ret_str)

    return matched_springs[0]

def delete_spring_visually(select_vgroups):
    for cur_group in select_vgroups:
        cnt = 0
        for vg in bpy.context.active_object.vertex_groups:        
            if cur_group == vg.name:
                cnt+=1
                bpy.context.active_object.vertex_groups.active_index = vg.index
        if cnt == 1:        
            bpy.ops.object.vertex_group_select()
    ob = bpy.context.active_object

    del_edge_list = []
    for edge in bm.edges:
        if edge.select:
            del_edge_list.append(edge.index)

    bpy.ops.mesh.select_all(action='DESELECT')

    bpy.ops.mesh.select_loose()

    loose_edge_list = []
    for edge in bm.edges:
        if edge.select:
            loose_edge_list.append(edge.index)

    bpy.ops.mesh.select_all(action='DESELECT')


    inter_del_edge_list = list(set.intersection(set(del_edge_list),set(loose_edge_list)))

    for edge in bm.edges:
        if edge.index in inter_del_edge_list:
            edge.select = True


    bpy.ops.mesh.delete(type='EDGE')


            
            
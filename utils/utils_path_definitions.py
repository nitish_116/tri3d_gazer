STATIC_PROPERTIES = {
    'KEYWORD_SETTINGS_CONFIG': 'settings_config',
    'KEYWORD_GARMENT_DEFAULT' : 'default',
}
def get_garment_folder(BASE_DIR):
    import bpy,os
    cur_garment_folder = bpy.context.scene.garment_folder
    garment_config_folder = BASE_DIR + STATIC_PROPERTIES['KEYWORD_SETTINGS_CONFIG'] +'/'+ cur_garment_folder + '/garment/' + STATIC_PROPERTIES['KEYWORD_GARMENT_DEFAULT'] + '/'
    # if not os.path.exists(garment_config_folder):
    #   ret_str = garment_config_folder + " folder does not exist"
    #   raise ValueError(ret_str)


    return garment_config_folder
    

def get_pattern_folder(BASE_DIR):
    import bpy
    cur_garment_folder = bpy.context.scene.garment_folder
    cur_pattern_folder = bpy.context.scene.pattern_folder
    pattern_config_folder = BASE_DIR + STATIC_PROPERTIES['KEYWORD_SETTINGS_CONFIG'] +'/'+ cur_garment_folder + '/pattern/' + cur_pattern_folder + '/'
    # if not os.path.exists(pattern_config_folder):
 #      ret_str = pattern_config_folder + " folder does not exist"
 #      raise ValueError(ret_str)
    return pattern_config_folder

def get_pattern_list(BASE_DIR):
    import os,bpy
    ans = []
    cur_garment_folder = bpy.context.scene.garment_folder
    pfolder = BASE_DIR + STATIC_PROPERTIES['KEYWORD_SETTINGS_CONFIG'] + '/' + cur_garment_folder + '/pattern/'
    
    for cur_pattern in os.listdir(pfolder):
        if 'default' != cur_pattern and '.DS_Store' != cur_pattern:
            ans.append(cur_pattern)
    return ans




def get_dynamic_properties(BASE_DIR):
    ans = {}
    ans['garment_folder_path'] = get_garment_folder(BASE_DIR)
    ans['pattern_folder_path'] = get_pattern_folder(BASE_DIR)
    ans['springs_dict_pickle_path'] = ans['garment_folder_path'] + 'springs.pickle'
    ans['positions_blend_path'] = ans['garment_folder_path'] + 'positions.blend'
    ans['edge_points_pickle_path'] = ans['garment_folder_path'] + 'edge_points.pickle'
    ans['outline_blend_path'] = ans['pattern_folder_path'] + 'outline.blend'
    ans['mesh_blend_path'] = ans['pattern_folder_path'] + 'mesh.blend'

    return ans 



